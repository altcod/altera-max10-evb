
module TOP_T1(
	input			CPLD_OSC_33V,  //pin 27, 50mhz
	input			C_VIN_OK,      //pin 86, j5.1, adruino_io7

	output reg	C_LED1n,
	output reg	C_LED2n,	
	output reg	C_LED3n,	
	output reg	C_LED4n,	
	output reg	C_LED5n,	
	
	output reg	TX2_PWR_BADn,	//pin 84, j5.2, adruino_io6.
	output reg  TX2_RST_OUT,	//pin 81, j5.3, adruino_io5.
	output reg  TX2_STM_CLK		//pin 79, j5.4, adruino_io4. 12.5mhz.
										//j3.2 gnd
	
);

//----------------------------------------------------------
localparam L=1'b0, H=1'b1; 

//===============================================================	

reg ledGREEN;
reg rstOUT, pwrBadOUT;

// Counter timeout
reg [31:0] cnt;
reg stm_CLK;
reg cntEn;

parameter c_cnt10ms= 10_000; //1 ms delay between active recovery and reset button
parameter c_cnt50ms=100_000; // 100ms
parameter c_cnt500ms=500_000; //500ms
parameter c_cnt200ms=200_000;
parameter c_cnt2s=3_000_000;
parameter c_cnt1000ms = 4_000_000;                      
                            
//=============================================================================

OPNDRN pwrBadOUT_inst (.in(!pwrBadOUT), .out(TX2_PWR_BADn));

//====================================================================
always @ (posedge CPLD_OSC_33V) begin
	begin
		stm_CLK = !stm_CLK;
	end
end
//====================================================================
//always @ (posedge stm_CLK or negedge cntEn) begin
always @ (posedge stm_CLK) begin
	if (!cntEn) 
		begin
			cnt = 0;
			cntEn = 1'b1;
		end
	else
		begin
			cnt = cnt + 1;
			TX2_STM_CLK = !TX2_STM_CLK;
		end
end		
//=============================================================================
always @ (posedge stm_CLK) begin
	//defaults
	ledGREEN = !ledGREEN; //L;
	///cntEn = L;
	rstOUT = L; //pwrBadOUT = H;
	
	if (cnt[15])  // cnt[22]=~2hz, cnt[9]==25/1.024khz, cnt[15]==25/64/1.024khz==381hz
		TX2_PWR_BADn = 1'bz;
	else
		TX2_PWR_BADn = 1'b0;
	
	///case (pmState)
	///endcase
	//post assignments
end
//=============================================================================
always @ (*) begin

	C_LED1n = !pwrBadOUT;  //; powerIn_ok; //!pwrBadOUT;
	C_LED2n = !ledGREEN; //!TX2_CARRIER_POWER_ON_OD_33V; //!ledGREEN;
	
	C_LED3n = cnt[22];
	C_LED4n = cnt[26];
	C_LED5n = cnt[30];
	
	//TX2_RST_OUT = !rstOUT; //this shows 4.2ms to high, with 17k initial pullup 
	TX2_RST_OUT = rstOUT; //this shows 4.2ms to low, with 17k initial pullup 
	
end
//=============================================================================

endmodule //TOP

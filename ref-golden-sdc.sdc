## Generated SDC file "TOP_T1.out.sdc"

## Copyright (C) 2016  Intel Corporation. All rights reserved.
## Your use of Intel Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Intel Program License 
## Subscription Agreement, the Intel Quartus Prime License Agreement,
## the Intel MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Intel and sold by Intel or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

## DATE    "Wed Feb 07 00:24:31 2018"

##
## DEVICE  "10M08SAE144C8G"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CPLD_OSC_33V} -period 20.000 -waveform { 0.000 10.000 } [get_ports {CPLD_OSC_33V}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {stm_CLK|q} -source [get_ports {CPLD_OSC_33V}] -divide_by 2 -master_clock {CPLD_OSC_33V} [get_nets {stm_CLK}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay  -clock [get_clocks {CPLD_OSC_33V}]  1.000 [get_ports {C_LED2n}]
set_output_delay -add_delay  -clock [get_clocks {CPLD_OSC_33V}]  1.000 [get_ports {C_LED3n}]
set_output_delay -add_delay  -clock [get_clocks {CPLD_OSC_33V}]  1.000 [get_ports {C_LED4n}]
set_output_delay -add_delay  -clock [get_clocks {CPLD_OSC_33V}]  1.000 [get_ports {C_LED5n}]
set_output_delay -add_delay  -clock [get_clocks {CPLD_OSC_33V}]  1.000 [get_ports {TX2_PWR_BADn}]
set_output_delay -add_delay  -clock [get_clocks {CPLD_OSC_33V}]  1.000 [get_ports {TX2_RST_OUT}]
set_output_delay -add_delay  -clock [get_clocks {CPLD_OSC_33V}]  1.000 [get_ports {TX2_STM_CLK}]


#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

